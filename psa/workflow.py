from sklearn.decomposition import PCA
from sklearn.decomposition import KernelPCA as kPCA
from sklearn.cluster import KMeans as KM
from sklearn import manifold
from sklearn.semi_supervised import label_propagation
from tsfresh.transformers import PerColumnImputer as PCI
from tsfresh import select_features
from scipy.spatial.distance import pdist, squareform
from scipy.stats.distributions import entropy
from scipy.optimize import basinhopping
from multiprocessing import Process, Queue
from mio.featureExtraction import timeSeriesAnalysis as tsa
from itertools import chain, combinations
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.widgets import TextBox
import numpy
import pandas
import gillespy
import time
import sys
import dask.distributed
import collections
import umap
import operator

# Todo - generalize
def set_model_parameters(model, params):
    """ params - a dict with parameter_name, value pairs. """
    for pname, p in model.listOfParameters.items():
        model.get_parameter(pname).set_expression(params[pname])

def draw_parameter_uniform(bounds):
    theta = collections.Counter()
    for name, val in bounds.items():
        drawnValue = numpy.random.random(1) * 2 - 1
        # scale from [0,1] to problem range
        min = val[0]
        max = val[1]
        #for j in range(0,len(scaledValue),1):
        scaledValue = abs((((drawnValue[0] + 1) * (max-min)) / 2) + min)
        theta[name] = scaledValue
    return theta

def sample_uniform(bounds,num_samples=1):
    thetas = []
    for i in range(num_samples):
        thetas.append(draw_parameter_uniform(bounds))
    return thetas

#The method that generates the synthetic data sets.
def simulate(parameters=None, model_file_location = None, timespan = None,
        timeout = 20.0,fc_params = None, default_fc_params=None, model=None):
    """ model_file_location: Location of the GillesPy model file, needs to 
        be accessible by all workers.
    """

    import gillespy 
    import numpy
    import time
    #from multiprocessing import Process, Queue

    if not model:
        model_doc = gillespy.StochMLDocument.from_file(model_file_location)
        model = model_doc.to_model("default_name")

    if not timespan.any():
        model.timespan(numpy.linspace(0,400,400))
    else:
        model.timespan(timespan)

    data = []

    #result = {"params":parameters}
    result = {}
    result["is_exploded"] = []
    simulated_parameters = []
    computed_samples = []

    t_start = time.time()
    # Simulate a batch
    for param in parameters:
        set_model_parameters(model,param)
        try:
            sim_res = model.run(timeout=timeout)[0]
            data.append(sim_res)
            computed_samples.append(param)
            result["is_exploded"].append(False)
        except gillespy.SolverTimeoutError:
            result["is_exploded"].append(True)

    t_end = time.time()
    result["runtime"]= t_end-t_start

    result["data"] = data
    result["params"] = computed_samples

    # If all realizations exploded, there is no data to extract features from
    if all(result["is_exploded"]):
        result["features"] = None
        result["feature_extraction_time"]=0.0
    else:
        tic = time.time()
        features, chunk_id = extract_ts_features(data,model.listOfSpecies.keys(),
                fc_params=fc_params, default_fc_params=default_fc_params)
        result["features"] = features
        result["feature_extraction_time"]=time.time()-tic

    return result

def extract_ts_features(data, columns,chunk_id = None, fc_params = None,
        default_fc_params = None):
    from mio.featureExtraction import timeSeriesAnalysis as tsa
    fe = tsa.TimeSeriesAnalysis(name='mixed_tspan', columns = columns)
    fe.put(data)
    fe.generate(fc_params=fc_params, default_fc_params = default_fc_params,
            progressbar_off=True)
    return fe.features, chunk_id

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

def propagate_labels(data, labels, kwargs):
    """Label propagation using Random Gaussian Fields and harmonic functions

       Zhu, Xiaojin and Ghahramani, Zoubin and Lafferty, John,
       Semi-supervised Learning Using Gaussian Fields and Harmonic Functions

       output: label distribution

    """
    model = label_propagation.LabelSpreading(**kwargs)
    model.fit(data, labels)
    return model

def get_label_entropies(label_distribution):

    return entropy(label_distribution.T)

def get_top_uncertain(label_distribution, unlabeled_indices, n = 10):
    """Get top n uncertain points according to entropy
    input:
    label_distribution: model.label_distributions_
    unlabeled_indices: i.e indices of those with -1 as label in original labels
    n = top n indices returned

    output:
    array of indices of top n, in decreasing order
    """
    pred_entropies = get_label_entropies(label_distribution)
    uncertainty_index = numpy.argsort(pred_entropies)[::-1]
    uncertainty_index = uncertainty_index[
            numpy.in1d(uncertainty_index, unlabeled_indices)][:n]
    return uncertainty_index


def get_average_label_entropy(label_distribution):

    return get_label_entropies(label_distribution).sum()/label_distribution.shape[0]

def optimize_features(data, labels, max_, min_, stepsize = 0.1, niter = 10):

    # taken from documentation
    class Bounds(object):
        def __init__(self, xmax, xmin):
            self.xmax = numpy.array(xmax)
            self.xmin = numpy.array(xmin)
        def __call__(self, **kwargs):
            x = kwargs["x_new"]
            tmax = bool(numpy.all(x <= self.xmax))
            tmin = bool(numpy.all(x >= self.xmin))
            return tmax and tmin

    #taken from Stackoverflow: https://stackoverflow.com/questions/21670080/how-to-find-global-minimum-in-python-optimization-with-bounds
    class RandomDisplacementBounds(object):
        """random displacement with bounds"""
        def __init__(self, xmax, xmin, stepsize=0.5):
            self.xmin = xmin
            self.xmax = xmax
            self.stepsize = stepsize

        def __call__(self, x):
            """take a random step but ensure the new position is within the bounds"""
            while True:
                # this could be done in a much more clever way,
                #but it will work for example purposes
                xnew = x + numpy.random.uniform(-self.stepsize, self.stepsize,
                        numpy.shape(x))
                if numpy.all(xnew < self.xmax) and numpy.all(xnew > self.xmin):
                    break
            return xnew


    def objective(x):
        return get_average_label_entropy(propagate_labels(data,
            labels, sigma = x).label_distributions_)


    step_routine = RandomDisplacementBounds(xmax = max_, xmin = min_,
            stepsize=stepsize)
    global_bounds = Bounds(xmax = max_, xmin = min_)
    start = numpy.random.uniform(min_, max_)

    minimizer_bounds = [(low, high) for low, high in zip(min_, max_)]

    minimizer_kwargs = dict(method = "L-BFGS-B", bounds = minimizer_bounds)

    res = basinhopping(objective, start, minimizer_kwargs=minimizer_kwargs,
                   niter=niter, accept_test=global_bounds,
                   take_step = step_routine, disp = True)
    return res

def do_tsne(data, nr_components = 2, init = 'random', plex = 30,
        n_iter = 1000, lr = 200, rs= None):

    tsne = manifold.TSNE(n_components=nr_components, init=init,
            perplexity=plex, random_state=rs, n_iter=n_iter, learning_rate=lr)

    return tsne.fit_transform(data), tsne

def do_pca(data, nr_components = 2, rs = None):
    pca = PCA(n_components=nr_components , random_state = rs)
    return pca.fit_transform(data), pca

def do_kpca(data , nr_components  = 2 , kernel  = 'rbf', gamma = 0.01,
        degree = 3):

    kpca = kPCA(n_components = nr_components, kernel = kernel, gamma = gamma,
            degree = degree)
    return kpca.fit_transform(data), kpca

def do_umap(data, nr_components = 2, nr_neighbors = 10, min_dist = 0.1):
    rd = umap.UMAP(n_components = nr_components, n_neighbors = nr_neighbors,
            min_dist = min_dist)
    return rd.fit_transform(data), rd


class WorkFlow():

    def __init__(self, ts_analysis, parameter_range, model_run, set_params, timeout=20):
        self.ts_analysis = ts_analysis
        self.estimators = []
        self.selected_features = []
        self.computed_samples = []
        self.exploded_samples = []
        self.all_samples = []

        self.run = model_run
        self.data = None
        self.minmax_scaler = None
        self.timeout = timeout
        self.runtime = []
        self.feature_extraction_time=[]
        self.batch_idx = [0,0]
        self.batch_history = []

        try:
            self.dask_client = dask.distributed.Client('127.0.0.1:8786')
        except:
            self.dask_client = None


    def simulate_candidates(self, model_file_location=None, timespan = None,
            fc_params=None, default_fc_params=None, batch_size = 50,timeout=20):
        """ Simulates all candidates from the sampling"""
        #if type(self.data) is numpy.ndarray:
        #    self.data = self.data.tolist()
        if isinstance(self.data, numpy.ndarray):
            c = len(self.data)
        else:
            c = 0
        #sys.stdout.write("Starting simulating candidates...\n")

        # TODO: Make use of chunking.
        clen = len(self.candidates)
        # Determine chunk size in smarter way
        chunked_candidates=list(chunks(self.candidates,batch_size))

        dask_client = self.dask_client

        tic = time.time()
        chunk_list_len = len(chunked_candidates)
        res = dask_client.map(simulate, chunked_candidates,
                [model_file_location]*chunk_list_len,
                [timespan]*chunk_list_len, [timeout]*chunk_list_len,
                [fc_params]*chunk_list_len, [default_fc_params]*chunk_list_len)
        rr = dask_client.gather(res)
        sim_time=time.time()-tic
        del res


        for e,r in enumerate(rr):

            params = r["params"]
            self.all_samples = self.all_samples + params

            for i, is_exploded in enumerate(r["is_exploded"]):
                if is_exploded:
                    self.exploded_samples.append(params[i])
            if r["data"] != []:
                if isinstance(self.data, numpy.ndarray):
                    try:
                        self.data = numpy.concatenate((self.data, r["data"]))
                    except ValueError:
                        print(numpy.shape(self.data), numpy.shape(r["data"]))
                        raise
                else:
                    self.data = numpy.array(r["data"])

            self.runtime.append(r["runtime"])
            self.feature_extraction_time.append(r["feature_extraction_time"])
            self.computed_samples.append(params)

            if isinstance(r["features"], pandas.DataFrame):
                self.ts_analysis.features = self.ts_analysis.features.append(r["features"], ignore_index=True)

            #sys.stdout.write("\r%d%% progress (nr exploaded samples: %d)" % ((float(e)/(len(self.candidates) -1))*100, len(self.exploded_samples)))
            #sys:.stdout.flush()

        c_after = len(self.data)
        #self.ts_analysis.put(self.data[c - c_after:]) # puts raw simulated data into feature extraction class and 
                                                      # dataframe container
        self.batch_idx[1] = c_after
        self.batch_history.append((len(self.candidates),c_after - c)) # keep track of history of: (sample size, nr_non-exploading_samples)

    def add_features(self, columns = None, default_fc_params = None, fc_params = None, batch_size=50):
        "Extract new features on existing/current data"
        chunked = list(chunks(self.data, batch_size))
        chunked_len = len(chunked)
        chunked_id = range(chunked_len)
        res = self.dask_client.map(extract_ts_features, chunked, [columns]*chunked_len,
                chunked_id, [fc_params]*chunked_len, [default_fc_params]*chunked_len)

        rr = self.dask_client.gather(res)
        rr.sort(key = operator.itemgetter(1))
        total_features = rr.pop(0)[0]
        for f in rr:
            total_features = total_features.append(f[0], ignore_index=True)

        self.ts_analysis.features = pandas.concat([self.ts_analysis.features, total_features], axis=1)

    def remove_index(self, indices = None):
        self.data = numpy.delete(self.data, indices, 0)
        self.ts_analysis.features = self.ts_analysis.features.drop(indices).reset_index(drop=True)
        self.batch_idx[1] -= len(indices)
        try:
             self.user_labels = numpy.delete(self.user_labels, indices)
        except:
            pass


    def generate_features(self, dask_client = None, fc_params = None,
            default_fc_params = None):
        """ Generate features. """

        if not fc_params:
            self.ts_analysis.generate(dask_client=None,progressbar_off=True, default_fc_params = default_fc_params)
        else:
            self.ts_analysis.generate(fc_params = fc_params,dask_client=None,progressbar_off=True, default_fc_params = default_fc_params)


    def pre_process(self,scaler=None):
        #Impute
        self.batch = PCI().fit(self.ts_analysis.features).transform(self.ts_analysis.features)
        self.batch = self.batch.as_matrix()[self.batch_idx[0]:self.batch_idx[1]]
        #Normalization
        if not scaler:
            scaler.fit(self.batch)
            self.batch = scaler.transform(self.batch)
        return scaler

    def visualize(self, skip_KM = False, skip_rd=False, rd = 'pca', title = "test", nr_clusters=10, species=None, kwargs=None):
            #Dimension reduction to 2D using a DR method
        if not skip_rd:
            sp_features, = numpy.where(self.ts_analysis.features.columns.str.match(species + '_'))
            if rd == 'tsne':
                self.data_rd, self.current_rd  = do_tsne(data=self.batch[:,sp_features], **kwargs)
            elif rd == 'pca':
                self.data_rd, self.current_rd  = do_pca(data=self.batch[:,sp_features], **kwargs)
            elif rd == 'kpca':
                self.data_rd, self.current_rd  = do_kpca(data=self.batch[:,sp_features], **kwargs)
            elif rd == 'umap': 
                self.data_rd, self.current_rd  = do_umap(data=self.batch[:,sp_features], **kwargs)
            else:
                raise ValueError(rd + " is not a supported DR method")


        if not skip_KM:
            #Clustering of 2D data
            km = KM(n_clusters=nr_clusters, init='random' )
            km.fit(self.data_rd)
            self.labels = km.labels_
            cluster_centers = km.cluster_centers_

        if not hasattr(self, 'user_labels'):
            self.user_labels = numpy.ones(len(self.batch),int)*-1

        if len(self.batch) > len(self.user_labels):
            self.user_labels = numpy.concatenate((self.user_labels, -1*numpy.ones(len(self.batch)-len(self.user_labels))))
        self.data = numpy.asarray(self.data)
        if rd in ['pca', 'kpca']:
            features, idx = self.get_top_features(method=rd)
            print "top 10 features involves:"
            print features
        self.interactive_plot(self.data_rd, range(len(self.data_rd)), self.labels, title, species)

    def get_top_features(self, method='pca', top_n_features = 10):
        if method == 'pca':
            components_sum = numpy.sum(self.pca.components_, axis=0)
        elif method == 'kpca':
            components_sum = numpy.sum(self.kpca.alphas_, axis=0)
        else:
            print "method not supported"
            return

        sorted_idx = numpy.argsort(components_sum)
        top_features = sorted_idx[-top_n_features:]
        return self.ts_analysis.get_fc_params(top_features), top_features

    def get_user_feedback(self):
        user_feedback = raw_input("Type preferred clusters: ")
        user_feedback = [int(cluster) for cluster in user_feedback.split()]

        for e, val in enumerate(self.labels):
            if val in user_feedback:
                self.labels[e] = 1
            else:
                self.labels[e] = 0

    def interactive_plot(self, data,indices,labels,title, species):

        def onpick3(event):
            ind = event.ind
            self.current_ind = ind
            text.set_position((data[ind,0], data[ind,1]))
            text.set_text(str(ind) + str(labels[ind])+ str(self.user_labels[ind]))

            #ts = ts[ind]
            ts = ts_data[ind].T[sp_idx] #species
            t = ts_data[ind].T[0] #timepoints

            maxi = str(int(max(ts)[0]))
            mini = str(int(min(ts)[0]))

            ax2.set_xlim(0,max(t))
            ax2.set_ylim(0,1.15)
            inc = max(t)/10
            ax2.set_xticks(numpy.arange(min(t),max(t)+inc, inc))

            ts = (ts - min(ts))/float(max(ts) - min(ts))
            ax2.set_yticks(numpy.arange(min(ts), max(ts)+.1, 0.5))

            time_s.set_visible(True)
            time_s.set_ydata(ts)
            time_s.set_xdata(t)

            arg_max =numpy.argmax(ts)
            arg_min = numpy.argmin(ts)

            max_plot.set_ydata(ts[arg_max])
            max_plot.set_xdata(t[arg_max])
            max_plot.set_label('Max: '+maxi)

            ax2.legend(loc='upper right', shadow=True)

            fig.canvas.draw()
            #label_ind = raw_input("Label selected point here: ")
            #self.user_labels[ind] = label_ind

                #print 'onpick3 scatter:', ind, indices[ind],labels[ind], np.take(data[:, 0], ind), np.take(data[:, 1], ind)
        N = len(numpy.unique(labels))
        ts_data = self.data[self.batch_idx[0]:self.batch_idx[1]]
        sp_idx = self.ts_analysis.data.columns.get_loc(species) - 2 # -2 since .data contains 2 extra columns
        if N < len(data)/2 :
            # define the colormap
            cmap = plt.cm.jet
            # extract all colors from the .jet map
            cmaplist = [cmap(i) for i in range(cmap.N)]
            # create the new map
            cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N)

            # define the bins and normalize
            bounds = numpy.linspace(0,N,N+1)
            norm = mpl.colors.BoundaryNorm(bounds, cmap.N)


        test = ts_data[0].T[sp_idx]

        fig, [[ax,ax2],[ax3,ax4]] = plt.subplots(2, 2, figsize=(15,7), gridspec_kw = {'width_ratios':[1, 1.3], 'height_ratios': [1, 0.1]})
        text = ax.text(0, 0, "", va="bottom", ha="left")
        text.set_text('test')


        if N < len(data)/2:
            line = ax.scatter(data[:, 0], data[:, 1], picker=True, c=labels,cmap=cmap,norm=norm)
        else:
            line = ax.scatter(data[:, 0], data[:, 1], picker=True, c=labels)
            plt.colorbar(line)
        time_s,  = ax2.plot(test, visible=True, alpha=0.7)

        max_plot, = ax2.plot(0,0,'ro', markersize= 12, label='Max')


        #ax.set_xlabel("tsne 1", style='italic', size=20)
        #ax.set_ylabel("tsne 2",style='italic', size=20)
        ax2.set_xlabel("time (hours)", style='italic', size=10)
        ax2.set_ylabel("copy number",style='italic', size=10)

        ax2.tick_params(labelsize=12)
        ax.tick_params(labelsize=12)
        max_data = int(max(data[:, 0]))
        min_data = int(min(data[:, 0]))
        #ax.set_xticks(numpy.arange(min_data,max_data + 1, (max_data - min_data)/5))

        max_data = int(max(data[:, 1]))
        min_data = int(min(data[:, 1]))
        #ax.set_yticks(numpy.arange(min_data,max_data + 1, (max_data - min_data)/5))

        def submit_idx(text):
            label = int(text)
            self.user_labels[self.current_ind] = label
            text_box_idx.set_val('')
            fig.canvas.draw()

        def submit_cluster(text):
            label = int(text)
            idxs, = numpy.where(labels == labels[self.current_ind])
            self.user_labels[idxs] = label
            text_box_cluster.set_val('')
            line.set_array(self.user_labels)
            fig.canvas.draw()


        #axbox = plt.axes([0.1, 0.01, 0.3, 0.05])
        text_box_idx = TextBox(ax3, 'Label point')
        text_box_cluster = TextBox(ax4, 'Label cluster')


        def enter_axes(event):
            text_box_idx.on_submit(submit_idx)
            text_box_cluster.on_submit(submit_cluster)
            event.canvas.draw()

        fig.canvas.mpl_connect('axes_enter_event', enter_axes)
        fig.canvas.mpl_connect('pick_event', onpick3)

        plt.show()
        #label_ind = raw_input("Label selected point here: ")
        #self.user_labels[self.current_ind] = label_ind


class InvalidDataTypeException(Exception):
    pass

